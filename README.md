![](https://imgs.xkcd.com/comics/success.png)
# SharkBait OS

A project to create a recognizable identity and foster development of portage-based Android.

https://www.shark-bait.org

## Official Mascot
![](theme/img/mako.png)

**Mako the Cyber Shark** is the mascot of SharkBait.

License: [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/) Copyright © Shark-Bait.

Download the hi-res of Mako [HERE](theme/img/mako_full-size.png)

Mascot character designed by **Tyson Tan**. Tyson Tan offers mascot design
service for free and open source software, free of charge, under free
license. Contact: [http://tysontan.com](http://tysontan.com) / [tysontan@mail.com](mailto:tysontan@mail.com)


See also:

- [Gentoo Linux](https://gentoo.org)

- [postmarketOS](https://postmarketos.org)

- [Ubuntu Touch](https://ubports.com)

- [LineageOS](https://lineageos.org)
