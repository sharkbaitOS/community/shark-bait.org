## Shark-Bait

A project to create a recognizable identity and foster development of [portage-powered][gsoc2018] [Android][android].

### Android

The mobile devices today become more powerful than desktop computers 10 years ago. As the hardware performance boosts, the software becomes a flexible part of the system. The pursuit of [software freedom][fsw] on desktop computers carries naturally to mobile ones.

The [Free Software Foundation][fsf] (FSF), has recognized the freedom for mobile computing early on. With a [GPL-licensed][gpl-v2] [Linux][linux] [kernel][kernel] and a [Apache-licensed][apache] [userland][userspace], Android is a big step forward for smartphones, tablets and multimedia centers. At the same time, as FSF points out, there is still a long way to go to regain full control of our own devices, "Smartphones are the most widely used form of personal computer today. Thus, the need for a fully free phone operating system is crucial to the proliferation of software freedom" ([Young, 2017][fsf-fp]).

Vendors have put effort to make their [source code open][osd]. However, it is rather challenging to figure out exactly how to build a usable system from these sources. [Patches][patch] for device-specific Linux kernels from vendors should be tracked. The [proprietary binary blobs][pure-blob] that drive certain hardware should be carefully analyzed and documented, and ultimately replaced with free versions. Sadly, most [bootloaders][coreboot] and [flashing tools][flashing] vary across vendors who impose these [artificial restrictions][closed-system] for either practical reasons or simply for profit.

### Variants

[LineageOS][lineage] (forked from Project [CyanogenMod][cm]) is another step forward in that its build system, finely [version controlled][vcs], tracks the details needed to build a working system. The development cycle of LineageOS is to [cross compile][cross-compiler], flash the image, then test. This is the most common development practice in the Android community. Although originally meaning "Read-Only Memory", _ROM_ in the Android context means the partitions of [flash memory][flash] that hold the Linux kernel and the Android userland. The same practice is taken by vendor-specific Android variants, and the original [Android Open Source Project][aosp] (AOSP). This practice eases the [software distribution][distribution] of big vendors, allowing them to have every single device install exactly the same partition images. But unfortunately for users, this imposes more artificial restrictions in updating and tinkering with the system.

With the advancement of mobile computing power, [native][native] compiling directly on the device is now feasible. It is possible to introduce [package management][package-management] to ROM development workflows, to bring the flexibility and ease of writing Android apps to building Android ROMs. This makes development and [porting][porting] more direct and easier for contributors. With the help of an additional [GNU userland][gnu], developing and testing on mobile devices will be no different from developing desktop computer applications, resulting in our phones becoming more [hackable][hacking] and more enjoyable to tweak.

### Gentoo

[Gentoo][gentoo] excels at managing build recipes with utmost clarity and elegance via [Portage][portage], Gentoo's official package manager. [Ebuilds][ebuild] are expressed in a self-documenting text format and therefore are most suitable for providing clarity in building Android from scratch. Gentoo has the potential to create a native development environment for mobile devices, making this vision a reality.

To achieve our ultimate goal of mobile computation freedom, three stages are required. First, a set of tools familiar to any [GNU/Linux][gnu/linux] user should be installed by the [Gentoo Prefix][gentoo-prefix]. Second, the Linux kernel should be recompiled and customized using the Gentoo Prefix tools by the user. Finally, the GNU userland should be able to access input/output peripherals (like HDMI video/audio output, USB keyboard, etc.) to provide an [X window system][x11]. Android and GNU userlands should be integrated by [message passing interfaces][mpi] and [framebuffer][framebuffer] copying and sharing. 

[android]: https://www.android.com
[aosp]: https://source.android.com
[apache]: https://www.apache.org/licenses
[closed-system]: https://en.wikipedia.org/wiki/Closed_platform
[cm]: https://en.wikipedia.org/wiki/CyanogenMod
[coreboot]:https://www.coreboot.org/users.html
[cross-compiler]: https://en.wikipedia.org/wiki/Cross_compiler
[distribution]: https://en.wikipedia.org/wiki/Software_distribution
[ebuild]: https://wiki.gentoo.org/wiki/Ebuild
[flash]: https://en.wikipedia.org/wiki/Flash_memory
[flashing]: https://en.wikipedia.org/wiki/Firmware#Flashing
[framebuffer]: https://wiki.gentoo.org/wiki/Framebuffer
[fsf]: https://www.fsf.org
[fsf-fp]: https://www.fsf.org/campaigns/priority-projects/free-phone
[fsw]: https://www.gnu.org/philosophy/free-sw.html
[gentoo]: https://www.gentoo.org
[gentoo-prefix]: https://wiki.gentoo.org/wiki/Project:Android
[gnu]: https://www.gnu.org/software
[gnu/linux]: https://www.gnu.org/gnu/linux-and-gnu.html
[gpl-v2]: https://www.gnu.org/licenses/gpl-2.0.html
[gsoc2018]: https://summerofcode.withgoogle.com/projects/#5132157995974656
[hacking]: http://www.catb.org/jargon/html/meaning-of-hack.html
[kernel]: https://en.wikipedia.org/wiki/Kernel_(operating_system)
[lineage]: https://lineageos.org
[linux]: https://www.kernel.org/linux.html
[mpi]: https://en.wikipedia.org/wiki/Message_Passing_Interface
[native]: https://en.wikipedia.org/wiki/Native_(computing)
[osd]: https://opensource.org/osd
[package-management]: https://en.wikipedia.org/wiki/Package_manager
[patch]: https://en.wikipedia.org/wiki/Patch_(computing)
[portage]: https://wiki.gentoo.org/wiki/Portage
[porting]: https://en.wikipedia.org/wiki/Porting
[pure-blob]: https://puri.sm/learn/blobs
[userspace]: https://en.wikipedia.org/wiki/User_space
[vcs]: https://en.wikipedia.org/wiki/Version_control
[x11]: https://en.wikipedia.org/wiki/X_Window_System
